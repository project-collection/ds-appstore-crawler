
# run standalone

  PYSPARK_PYTHON=/Users/wu/anaconda3/envs/knime/bin/python spark-submit \
  --conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=/Users/wu/anaconda3/envs/knime/bin/python \
  --master local \
  --archives /Users/wu/IdeaProjects/AppStoreCrawler.zip#ANACONDA \
  /Users/wu/IdeaProjects/AppStoreCrawler/GooglePlaySitemapCrawler.py


 # run on yarn
 #PYSPARK_PYTHON=/Users/wu/anaconda3/envs/knime/bin/python spark-submit \
 #--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=/Users/wu/anaconda3/envs/knime/bin/python \
 #--master yarn-cluster \
 #--archives /Users/wu/IdeaProjects/AppStoreCrawler.zip#ANACONDA \
 #/Users/wu/IdeaProjects/AppStoreCrawler/GooglePlaySitemapCrawler.py


# PYTHON_PATH
# /Users/wu/anaconda3/envs/knime/bin/python
# zip file
# /Users/wu/IdeaProjects/AppStoreCrawler.zip