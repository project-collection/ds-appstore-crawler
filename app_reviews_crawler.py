import bs4
from urllib.request import urlopen
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import pandas as pd

# Firefox driver download https://github.com/mozilla/geckodriver/releases
# AttributeError: 'Options' object has no attribute 'binary'  https://github.com/SeleniumHQ/selenium/issues/4643
def get_simulated_driver(url):


    options = Options()
    options.add_argument("--headless")
    # options.add_argument("window-size=1920,1080")
    #options.profile = fp
    # options.add_argument('--no-sandbox')
    # options.add_argument('--ignore-certificate-errors')
    driver = webdriver.Firefox(firefox_options=options)
    driver.get(url)
    return driver

def get_html(url):
    response = urlopen(url)
    html = response.read().decode("utf-8")
    return html

def get_appId_list():
    telekom_app_id_list = [#"de.telekom.mail",
                           "de.telekom.android.customercenter",
                           "de.telekom.hotspotlogin.de",
                           "de.telekom.android.dslhilfe",
                           "de.telekom.basketball",
                           "de.telekom.mds.mbp",
                           "de.telekom.entertaintv.smartphone",
                           "de.telekom.smarthomeb2c"
                           ]
    one_and_one_app_id_list = [#"com.oneandone.controlcenter",
                               #"de.eue.mobile.android.mail",
                               #"de.einsundeins.dokumente",
                               "de.einsundeins.smartdrive",
                               "de.einsundeins.homephone",
                               "de.einsundeins.homenet",
                               "com.oneandone.access.apps.musicplayer",
                               "com.oneandone.ciso.mobile.app.android",
                               "com.einsundeinstv.player",
                               "de.eue.mobile.android.tresor"
                               ]
    return telekom_app_id_list
    # return one_and_one_app_id_list + telekom_app_id_list
    #return ["de.telekom.basketball"]



"""
Extremely strong dependency on web html
"""
def scrap_reviews_from_single_webpage(soup):
    """
    parser: html.parser, html5lib, lxml

    return a list of reviews
    Input String HTML page
    Output List  reivews
    """

    for single_review in soup.select("div.d15Mdf"):
        try:
            review = {}
            review["author_name"] = single_review.select("span.X43Kjb")[0].text
            review["review_date"] = single_review.select("span.p2TkOb")[0].text
            review["review_body"] = single_review.select("div.UD7Dzf")[0].span.text
            #review["review_response"] = single_review.select("div.UD7Dzf")[0].span.text
            review["review_star"] = single_review.select("div.pf5lIe")[0].div.get("aria-label")
            yield review
        except:
            continue



def infinite_click(driver):
    SCROLL_PAUSE_TIME = 1.5
    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")

    i = 0
    while True:
        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(SCROLL_PAUSE_TIME)
        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            try:
                # bottom function
                play_button = driver.find_elements_by_class_name("RveJvd")[0]
                play_button.click()
            except:
                break
        last_height = new_height
        i = i+1
        if i%10==0:
            print(i)



def scrap_google_play_single_webpage(driver):
    infinite_click(driver)
    print("Infinite click finished")
    html = driver.page_source
    return html

def get_reviews(html):
    soup = bs4.BeautifulSoup(html, 'html.parser')
    reviews = [review for review in scrap_reviews_from_single_webpage(soup)]
    return reviews


def main():
    for app_id in get_appId_list():
        url = "https://play.google.com/store/apps/details?id="+app_id+"&hl=de&showAllReviews=true"
        driver = get_simulated_driver(url)
        # app-reviews
        html = scrap_google_play_single_webpage(driver)
        driver.quit()
        reviews = pd.DataFrame(get_reviews(html))
        reviews["review_star"] = reviews["review_star"].apply(lambda x:x.split()[1])
        print(reviews.shape)
        reviews.to_csv("../Data/google_play_data/reviews_"+app_id+".csv")


if __name__ == "__main__":
    main()
