# Description

Crawl app data, reviews from google app website (https://play.google.com/store/apps). Due to the massive amount of apps, the script is parallelied with Spark.
The Crawling Program is deployed on AWS cloud.

# Technology

* pyspark
* python
* pandas
* selenium
