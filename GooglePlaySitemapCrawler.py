import bs4
from urllib.request import urlopen, URLError
import gzip
from pyspark import SparkContext
from pyspark import SparkConf


def wget(url):
    """ Download zipped xml package
    :param url: from play store sitemap (https://play.google.com/sitemaps/sitemaps-index-0.xml)
    :type url: String
    :return: file
    :rtype: Bytes
    """

    attempts = 0
    while attempts < 3:
        try:
            response = urlopen(url, timeout=5)
            file = response.read()

            print("Downloaded")
            break
        except URLError as e:
            attempts += 1
    return file


def unzip(file):
    """ Uncompress .gz file
    :param file: .gz file
    :return soup: soup object
    """
    # unzip .gz file
    xml_content = gzip.decompress(file)
    soup = bs4.BeautifulSoup(xml_content, 'lxml')
    return soup


def parse_xml(soup):
    """ Extract play store resource links
    :param soup: xml content
    :type soup: BeautifulSoup object
    :return: list of resource links
    """
    items = [url.loc.text for url in soup.find_all("url")]
    return items

def process(url):
    """ Ensemble functions
    :param url: input of wget
    :return: result of parse_xml
    """
    file = wget(url)
    soup = unzip(file)
    items = parse_xml(soup)
    return items


def main():
    """ Spark Application
    Todo:
        * input instead of Example urls
    """
    conf = SparkConf()
    conf.setAppName("get-hosts")
    sc = SparkContext(conf=conf)
    urls = ["https://play.google.com/sitemaps/play_sitemaps_2018-03-24_1521950406-00000-of-54392.xml.gz",
            "https://play.google.com/sitemaps/play_sitemaps_2018-03-24_1521950406-00001-of-54392.xml.gz"]
    rdd = sc.parallelize(urls)
    resources = rdd.map(process).collect()
    # flatten
    flatten_resources = rdd.flatMap(lambda xs: [x[0] for x in resources]).collect()
    print("Sample output:")
    print(flatten_resources[:5])


main()